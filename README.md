# General-Purpose LaTeX Helpers (gp-tex)

### How to build the documentation and set up the texmf folder structure
You can either build the documentation using the provided gradle build script or build it 'manually' with LaTeX.
#### Building using Gradle:
1. Download and install [TeXlive](https://www.tug.org/texlive/)
2. Download and install `gradle` and `poppler-utils`
3. clone this repository
4. (optional) Download and install IntelliJ and the TeXiFy-IDEA LaTeX plugin and create a LaTeX `project from existing sources`. Select root folder of this repo as project directory. Click `Import Gradle Build`
5. Execute the Gradle task `copy_texmf` in the command line or in IntelliJ
6. Copy the `tex` folder that was created in the `out/` directory into your `texmf` system folder or make the `out/` folder itself a `texmf` folder
7. Run `gradle tex`
8. You will find the pdf output files in the `out/` directory of the repository.
#### Building documents using GitLab CI or Docker
You can find an example CI configuration in the [GitLab CI YML Config file](.gitlab-ci.yml) of this repository.
        You can copy its contents to your project and change it to your needs.
        The required Dockerfile can be found [here](Dockerfile) in the repository or in the repository's Gitlab registry.
#### Building manually
1. Set up your `texmf` folder as shown above
1. Locate and open the desired documentation document found in `src/manuals` in a TeX editor of your choice
2. build the document. You will find the pdf output in the same folder as the source file. Make sure to build the TOC as well.

## License
Copyright 2015-2020 Lukas Kirschner

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
