#!/bin/sh

STY_ROOT_NAME="texmf"
cd src/headers
STY_FILES=$(ls -1 *.sty | sed -e 's/\.sty$//')
CLS_FILES=$(ls -1 *.cls | sed -e 's/\.cls$//')
cd ../..
for PREFIX in $STY_FILES; do
  mkdir -p ${STY_ROOT_NAME}/tex/latex/${PREFIX}
  cp -v src/headers/${PREFIX}.sty ${STY_ROOT_NAME}/tex/latex/${PREFIX}
done
for PREFIX in $CLS_FILES; do
  mkdir -p ${STY_ROOT_NAME}/tex/latex/base
  cp -v src/headers/${PREFIX}.cls ${STY_ROOT_NAME}/tex/latex/base
done
printf "All done. Copied files to %s\n" ${STY_ROOT_NAME}