%This file is a modified version of the book.cls file from the LaTeX base package.
%It automatically sets up an environment including all necessary LK headers
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{gpclass}
              [2014/09/29 v1.0 LK Document Template based on book.cls]
\newlength{\nomarginshift}
\setlength{\nomarginshift}{-.25in}
\newcommand\@ptsize{}
\newif\if@restonecol
\newif\if@titlepage
\@titlepagetrue
\newif\if@openright
\newif\if@mainmatter \@mainmattertrue
\if@compatibility\else
\DeclareOption{a4paper}
   {\setlength\paperheight {297mm}%
    \setlength\paperwidth  {210mm}}
\DeclareOption{a5paper}
   {\setlength\paperheight {210mm}%
    \setlength\paperwidth  {148mm}}
\DeclareOption{b5paper}
   {\setlength\paperheight {250mm}%
    \setlength\paperwidth  {176mm}}
\DeclareOption{letterpaper}
   {\setlength\paperheight {11in}%
    \setlength\paperwidth  {8.5in}}
\DeclareOption{legalpaper}
   {\setlength\paperheight {14in}%
    \setlength\paperwidth  {8.5in}}
\DeclareOption{executivepaper}
   {\setlength\paperheight {10.5in}%
    \setlength\paperwidth  {7.25in}}%---------OWN DOCUMENT SIZES BEGIN
\DeclareOption{kindle}
   {\setlength\paperheight {115mm}%
    \setlength\paperwidth  {90mm}
   \AtEndOfPackage{%
   \addtolength{\oddsidemargin}{\nomarginshift}
   \addtolength{\evensidemargin}{\nomarginshift}
   \addtolength{\textwidth}{-2\nomarginshift}
   \addtolength{\topmargin}{.1\nomarginshift}
   \addtolength{\textheight}{-2.5\nomarginshift}
   \addtolength{\footskip}{-.5\nomarginshift}
   \setlength{\headsep}{12pt}
   \renewcommand{\baselinestretch}{0.95}
   }}
\DeclareOption{nexus6}
   {\setlength\paperheight {131mm}%
    \setlength\paperwidth  {74mm}
   \AtEndOfPackage{%
    \addtolength{\oddsidemargin}{\nomarginshift}
    \addtolength{\evensidemargin}{\nomarginshift}
    \addtolength{\textwidth}{-2\nomarginshift}
    \addtolength{\topmargin}{.3\nomarginshift}
    \addtolength{\textheight}{-3\nomarginshift}
    \addtolength{\footskip}{-.5\nomarginshift}
    \setlength{\headsep}{12pt}
    \renewcommand{\baselinestretch}{0.95}
   }}
\DeclareOption{landscape}
   {\setlength\@tempdima   {\paperheight}%
    \setlength\paperheight {\paperwidth}%
    \setlength\paperwidth  {\@tempdima}}
\fi
\if@compatibility
  \renewcommand\@ptsize{0}
\else
\DeclareOption{10pt}{\renewcommand\@ptsize{0}}
\fi
\DeclareOption{11pt}{\renewcommand\@ptsize{1}}
\DeclareOption{12pt}{\renewcommand\@ptsize{2}}
\if@compatibility\else
\DeclareOption{oneside}{\@twosidefalse \@mparswitchfalse}
\fi
\DeclareOption{twoside}{\@twosidetrue  \@mparswitchtrue}
\DeclareOption{draft}{\setlength\overfullrule{5pt}}
\if@compatibility\else
\DeclareOption{final}{\setlength\overfullrule{0pt}}
\fi
%\DeclareOption{titlepage}{\@titlepagetrue}
%\if@compatibility\else
%\DeclareOption{notitlepage}{\@titlepagefalse}
%\fi
\if@compatibility
\@openrighttrue
\else
\DeclareOption{openright}{\@openrighttrue}
\DeclareOption{openany}{\@openrightfalse}
\fi
\if@compatibility\else
\DeclareOption{onecolumn}{\@twocolumnfalse}
\fi
\DeclareOption{twocolumn}{\@twocolumntrue}
\DeclareOption{leqno}{\input{leqno.clo}}
\DeclareOption{fleqn}{\input{fleqn.clo}}
\DeclareOption{openbib}{%
  \AtEndOfPackage{%
   \renewcommand\@openbib@code{%
      \advance\leftmargin\bibindent
      \itemindent -\bibindent
      \listparindent \itemindent
      \parsep \z@
      }%
   \renewcommand\newblock{\par}}%
}
\ExecuteOptions{a4paper,11pt,oneside,onecolumn,final,openright}%STANDARD OPTIONS
\ProcessOptions


\input{bk1\@ptsize.clo}
\setlength\lineskip{1\p@}
\setlength\normallineskip{1\p@}
\renewcommand\baselinestretch{}
\setlength\parskip{0\p@ \@plus \p@}
\@lowpenalty   51
\@medpenalty  151
\@highpenalty 301
\setcounter{topnumber}{2}
\renewcommand\topfraction{.7}
\setcounter{bottomnumber}{1}
\renewcommand\bottomfraction{.3}
\setcounter{totalnumber}{3}
\renewcommand\textfraction{.2}
\renewcommand\floatpagefraction{.5}
\setcounter{dbltopnumber}{2}
\renewcommand\dbltopfraction{.7}
\renewcommand\dblfloatpagefraction{.5}
\if@twoside
  \def\ps@headings{%
      \let\@oddfoot\@empty\let\@evenfoot\@empty
      \def\@evenhead{\thepage\hfil\slshape\leftmark}%
      \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
      \let\@mkboth\markboth
    \def\chaptermark##1{%
      \markboth {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
          \if@mainmatter
            \@chapapp\ \thechapter. \ %
          \fi
        \fi
        ##1}}{}}%
    \def\sectionmark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@secnumdepth >\z@
          \thesection. \ %
        \fi
        ##1}}}}
\else
  \def\ps@headings{%
    \let\@oddfoot\@empty
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\markboth
    \def\chaptermark##1{%
      \markright {\MakeUppercase{%
        \ifnum \c@secnumdepth >\m@ne
          \if@mainmatter
            \@chapapp\ \thechapter. \ %
          \fi
        \fi
        ##1}}}}
\fi
\def\ps@myheadings{%
    \let\@oddfoot\@empty\let\@evenfoot\@empty
    \def\@evenhead{\thepage\hfil\slshape\leftmark}%
    \def\@oddhead{{\slshape\rightmark}\hfil\thepage}%
    \let\@mkboth\@gobbletwo
    \let\chaptermark\@gobble
    \let\sectionmark\@gobble
    }
  \if@titlepage
  \newcommand\maketitle{\begin{titlepage}%
  \let\footnotesize\small
  \let\footnoterule\relax
  \let \footnote \thanks
  \null\vfil
  \vskip 60\p@
  \begin{center}%
    {\LARGE \@title \par}%
    \vskip 3em%
    {\large
     \lineskip .75em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
      \vskip 1.5em%
    {\large \@date \par}%       % Set date in \large size.
  \end{center}\par
  \@thanks
  \vfil\null
  \end{titlepage}%
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\else
\newcommand\maketitle{\par
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \if@twocolumn
      \ifnum \col@number=\@ne
        \@maketitle
      \else
        \twocolumn[\@maketitle]%
      \fi
    \else
      \newpage
      \global\@topnum\z@   % Prevents figures from going at top of page.
      \@maketitle
    \fi
    \thispagestyle{plain}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}
\def\@maketitle{%
  \newpage
  \null
  \vskip 2em%
  \begin{center}%
  \let \footnote \thanks
    {\LARGE \@title \par}%
    \vskip 1.5em%
    {\large
      \lineskip .5em%
      \begin{tabular}[t]{c}%
        \@author
      \end{tabular}\par}%
    \vskip 1em%
    {\large \@date}%
  \end{center}%
  \par
  \vskip 1.5em}
\fi
\newcommand*\chaptermark[1]{}
\setcounter{secnumdepth}{2}
\newcounter{part}
\newcounter{chapter}
\newcounter{section}[chapter]
\newcounter{subsection}[section]
\newcounter{subsubsection}[subsection]
\newcounter{paragraph}[subsubsection]
\newcounter{subparagraph}[paragraph]
\renewcommand\thepart {\@Roman\c@part}
\renewcommand\thechapter {\@arabic\c@chapter}
%\renewcommand \thesection {\thechapter.\@arabic\c@section}
\renewcommand\thesection {\@arabic\c@section} % We want single numbers as section numbering instead of "chapterx.123"
\renewcommand\thesubsection   {\thesection.\@arabic\c@subsection}
\renewcommand\thesubsubsection{\thesubsection.\@arabic\c@subsubsection}
\renewcommand\theparagraph    {\thesubsubsection.\@arabic\c@paragraph}
\renewcommand\thesubparagraph {\theparagraph.\@arabic\c@subparagraph}
\newcommand\@chapapp{\chaptername}
\newcommand\frontmatter{%
    \cleardoublepage
  \@mainmatterfalse
  \pagenumbering{roman}}
\newcommand\mainmatter{%
    \cleardoublepage
  \@mainmattertrue
  \pagenumbering{arabic}}
\newcommand\backmatter{%
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \@mainmatterfalse}
\newcommand\part{%
  \if@openright
    \cleardoublepage
  \else
    \clearpage
  \fi
  \thispagestyle{plain}%
  \if@twocolumn
    \onecolumn
    \@tempswatrue
  \else
    \@tempswafalse
  \fi
  \null\vfil
  \secdef\@part\@spart}

\def\@part[#1]#2{%
    \ifnum \c@secnumdepth >-2\relax
      \refstepcounter{part}%
      \addcontentsline{toc}{part}{\thepart\hspace{1em}#1}%
    \else
      \addcontentsline{toc}{part}{#1}%
    \fi
    \markboth{}{}%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \ifnum \c@secnumdepth >-2\relax
       \huge\bfseries \partname\nobreakspace\thepart
       \par
       \vskip 20\p@
     \fi
     \Huge \bfseries #2\par}%
    \@endpart}
\def\@spart#1{%
    {\centering
     \interlinepenalty \@M
     \normalfont
     \Huge \bfseries #1\par}%
    \@endpart}
\def\@endpart{\vfil\newpage
              \if@twoside
               \if@openright
                \null
                \thispagestyle{empty}%
                \newpage
               \fi
              \fi
              \if@tempswa
                \twocolumn
              \fi}
\newcommand\chapter{\if@openright\cleardoublepage\else\clearpage\fi
                    \thispagestyle{plain}%
                    \global\@topnum\z@
                    \@afterindentfalse
                    \secdef\@chapter\@schapter}
\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
                       \if@mainmatter
                         \refstepcounter{chapter}%
                         \typeout{\@chapapp\space\thechapter.}%
                         \addcontentsline{toc}{chapter}%
                                   {\protect\numberline{\thechapter}#1}%
                       \else
                         \addcontentsline{toc}{chapter}{#1}%
                       \fi
                    \else
                      \addcontentsline{toc}{chapter}{#1}%
                    \fi
                    \chaptermark{#1}%
                    \addtocontents{lof}{\protect\addvspace{10\p@}}%
                    \addtocontents{lot}{\protect\addvspace{10\p@}}%
                    \if@twocolumn
                      \@topnewpage[\@makechapterhead{#2}]%
                    \else
                      \@makechapterhead{#2}%
                      \@afterheading
                    \fi}
\def\@makechapterhead#1{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright \normalfont
    \ifnum \c@secnumdepth >\m@ne
      \if@mainmatter
        \huge\bfseries \@chapapp\space \thechapter
        \par\nobreak
        \vskip 20\p@
      \fi
    \fi
    \interlinepenalty\@M
    \Huge \bfseries #1\par\nobreak
    \vskip 40\p@
  }}
\def\@schapter#1{\if@twocolumn
                   \@topnewpage[\@makeschapterhead{#1}]%
                 \else
                   \@makeschapterhead{#1}%
                   \@afterheading
                 \fi}
\def\@makeschapterhead#1{%
  \vspace*{50\p@}%
  {\parindent \z@ \raggedright
    \normalfont
    \interlinepenalty\@M
    \Huge \bfseries  #1\par\nobreak
    \vskip 40\p@
  }}
\newcommand\section{\@startsection {section}{1}{\z@}%
                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\Large\bfseries}}
\newcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\large\bfseries}}
\newcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
\newcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
\newcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}
\if@twocolumn
  \setlength\leftmargini  {2em}
\else
  \setlength\leftmargini  {2.5em}
\fi
\leftmargin  \leftmargini
\setlength\leftmarginii  {2.2em}
\setlength\leftmarginiii {1.87em}
\setlength\leftmarginiv  {1.7em}
\if@twocolumn
  \setlength\leftmarginv  {.5em}
  \setlength\leftmarginvi {.5em}
\else
  \setlength\leftmarginv  {1em}
  \setlength\leftmarginvi {1em}
\fi
\setlength  \labelsep  {.5em}
\setlength  \labelwidth{\leftmargini}
\addtolength\labelwidth{-\labelsep}
\@beginparpenalty -\@lowpenalty
\@endparpenalty   -\@lowpenalty
\@itempenalty     -\@lowpenalty

%%
%% Enum Items and Enumerate Bullets
%%

\renewcommand\theenumi{\@arabic\c@enumi}
\renewcommand\theenumii{\@alph\c@enumii}
\renewcommand\theenumiii{\@roman\c@enumiii}
\renewcommand\theenumiv{\@Alph\c@enumiv}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{\textbullet}
\newcommand\labelitemii{\normalfont\bfseries \textendash}
\newcommand\labelitemiii{\textasteriskcentered}
\newcommand\labelitemiv{\textperiodcentered}

%%
%% Special Environments
%%

\newenvironment{description}
               {\list{}{\labelwidth\z@ \itemindent-\leftmargin
                        \let\makelabel\descriptionlabel}}
               {\endlist}
\newcommand*\descriptionlabel[1]{\hspace\labelsep
                                \normalfont\bfseries #1}
\newenvironment{verse}
               {\let\\\@centercr
                \list{}{\itemsep      \z@
                        \itemindent   -1.5em%
                        \listparindent\itemindent
                        \rightmargin  \leftmargin
                        \advance\leftmargin 1.5em}%
                \item\relax}
               {\endlist}
\newenvironment{quotation}
               {\list{}{\listparindent 1.5em%
                        \itemindent    \listparindent
                        \rightmargin   \leftmargin
                        \parsep        \z@ \@plus\p@}%
                \item\relax}
               {\endlist}
\newenvironment{quote}
               {\list{}{\rightmargin\leftmargin}%
                \item\relax}
               {\endlist}
\if@compatibility
\newenvironment{titlepage}
    {%
      \cleardoublepage
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      \setcounter{page}\z@
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
    }
\else
\newenvironment{titlepage}
    {%
      \cleardoublepage
      \if@twocolumn
        \@restonecoltrue\onecolumn
      \else
        \@restonecolfalse\newpage
      \fi
      \thispagestyle{empty}%
      \setcounter{page}\@ne
    }%
    {\if@restonecol\twocolumn \else \newpage \fi
     \if@twoside\else
        \setcounter{page}\@ne
     \fi
    }
\fi
\newcommand\appendix{\par
  \setcounter{chapter}{0}%
  \setcounter{section}{0}%
  \gdef\@chapapp{\appendixname}%
  \gdef\thechapter{\@Alph\c@chapter}}
\setlength\arraycolsep{5\p@}
\setlength\tabcolsep{6\p@}
\setlength\arrayrulewidth{.4\p@}
\setlength\doublerulesep{2\p@}
\setlength\tabbingsep{\labelsep}
\skip\@mpfootins = \skip\footins
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}
\@addtoreset {equation}{chapter}
\renewcommand\theequation
  {\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@equation}
\newcounter{figure}[chapter]
\renewcommand \thefigure
     {\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@figure}
\def\fps@figure{tbp}
\def\ftype@figure{1}
\def\ext@figure{lof}
\def\fnum@figure{\figurename\nobreakspace\thefigure}
\newenvironment{figure}
               {\@float{figure}}
               {\end@float}
\newenvironment{figure*}
               {\@dblfloat{figure}}
               {\end@dblfloat}
\newcounter{table}[chapter]
\renewcommand \thetable
     {\ifnum \c@chapter>\z@ \thechapter.\fi \@arabic\c@table}
\def\fps@table{tbp}
\def\ftype@table{2}
\def\ext@table{lot}
\def\fnum@table{\tablename\nobreakspace\thetable}
\newenvironment{table}
               {\@float{table}}
               {\end@float}
\newenvironment{table*}
               {\@dblfloat{table}}
               {\end@dblfloat}
\newlength\abovecaptionskip
\newlength\belowcaptionskip
\setlength\abovecaptionskip{10\p@}
\setlength\belowcaptionskip{0\p@}
\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \sbox\@tempboxa{#1: #2}%
  \ifdim \wd\@tempboxa >\hsize
    #1: #2\par
  \else
    \global \@minipagefalse
    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
  \fi
  \vskip\belowcaptionskip}
\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\DeclareRobustCommand*\cal{\@fontswitch\relax\mathcal}
\DeclareRobustCommand*\mit{\@fontswitch\relax\mathnormal}
\newcommand\@pnumwidth{1.55em}
\newcommand\@tocrmarg{2.55em}
\newcommand\@dotsep{4.5}
\setcounter{tocdepth}{2}
\newcommand\tableofcontents{%
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \chapter*{\contentsname
        \@mkboth{%
           \MakeUppercase\contentsname}{\MakeUppercase\contentsname}}%
    \@starttoc{toc}%
    \if@restonecol\twocolumn\fi
    }
\newcommand*\l@part[2]{%
  \ifnum \c@tocdepth >-2\relax
    \addpenalty{-\@highpenalty}%
    \addvspace{2.25em \@plus\p@}%
    \setlength\@tempdima{3em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      {\leavevmode
       \large \bfseries #1\hfil \hb@xt@\@pnumwidth{\hss #2}}\par
       \nobreak
         \global\@nobreaktrue
         \everypar{\global\@nobreakfalse\everypar{}}%
    \endgroup
  \fi}
\newcommand*\l@chapter[2]{%
  \ifnum \c@tocdepth >\m@ne
    \addpenalty{-\@highpenalty}%
    \vskip 1.0em \@plus\p@
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
      \penalty\@highpenalty
    \endgroup
  \fi}
\newcommand*\l@section{\@dottedtocline{1}{1.5em}{2.3em}}
\newcommand*\l@subsection{\@dottedtocline{2}{3.8em}{3.2em}}
\newcommand*\l@subsubsection{\@dottedtocline{3}{7.0em}{4.1em}}
\newcommand*\l@paragraph{\@dottedtocline{4}{10em}{5em}}
\newcommand*\l@subparagraph{\@dottedtocline{5}{12em}{6em}}
\newcommand\listoffigures{%
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \chapter*{\listfigurename}%
      \@mkboth{\MakeUppercase\listfigurename}%
              {\MakeUppercase\listfigurename}%
    \@starttoc{lof}%
    \if@restonecol\twocolumn\fi
    }
\newcommand*\l@figure{\@dottedtocline{1}{1.5em}{2.3em}}
\newcommand\listoftables{%
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \chapter*{\listtablename}%
      \@mkboth{%
          \MakeUppercase\listtablename}%
         {\MakeUppercase\listtablename}%
    \@starttoc{lot}%
    \if@restonecol\twocolumn\fi
    }
\let\l@table\l@figure
\newdimen\bibindent
\setlength\bibindent{1.5em}
\newenvironment{thebibliography}[1]
     {\chapter*{\bibname}%
      \@mkboth{\MakeUppercase\bibname}{\MakeUppercase\bibname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\newcommand\newblock{\hskip .11em\@plus.33em\@minus.07em}
\let\@openbib@code\@empty
\newenvironment{theindex}
               {\if@twocolumn
                  \@restonecolfalse
                \else
                  \@restonecoltrue
                \fi
                \twocolumn[\@makeschapterhead{\indexname}]%
                \@mkboth{\MakeUppercase\indexname}%
                        {\MakeUppercase\indexname}%
                \thispagestyle{plain}\parindent\z@
                \parskip\z@ \@plus .3\p@\relax
                \columnseprule \z@
                \columnsep 35\p@
                \let\item\@idxitem}
               {\if@restonecol\onecolumn\else\clearpage\fi}
\newcommand\@idxitem{\par\hangindent 40\p@}
\newcommand\subitem{\@idxitem \hspace*{20\p@}}
\newcommand\subsubitem{\@idxitem \hspace*{30\p@}}
\newcommand\indexspace{\par \vskip 10\p@ \@plus5\p@ \@minus3\p@\relax}
\renewcommand\footnoterule{%
  \kern-3\p@
  \hrule\@width.4\columnwidth
  \kern2.6\p@}
\@addtoreset{footnote}{chapter}
\newcommand\@makefntext[1]{%
    \parindent 1em%
    \noindent
    \hb@xt@1.8em{\hss\@makefnmark}#1}
\newcommand\contentsname{Contents}
\newcommand\listfigurename{List of Figures}
\newcommand\listtablename{List of Tables}
\newcommand\bibname{Bibliography}
\newcommand\indexname{Index}
\newcommand\figurename{Figure}
\newcommand\tablename{Table}
\newcommand\partname{Part}
\newcommand\chaptername{Chapter}
\newcommand\appendixname{Appendix}
\def\today{\ifcase\month\or
  January\or February\or March\or April\or May\or June\or
  July\or August\or September\or October\or November\or December\fi
  \space\number\day, \number\year}
\setlength\columnsep{10\p@}
\setlength\columnseprule{0\p@}
\pagestyle{headings}
\pagenumbering{arabic}
\if@twoside
\else
  \raggedbottom
\fi
\if@twocolumn
  \twocolumn
  \sloppy
  \flushbottom
\else
  \onecolumn
\fi


%OLD LKPAGESTYLE MODIFICATIONS GO HERE...
%
%\newlength{\gppagewidth}
%\setlength{\gppagewidth}{\textwidth}
%\newcommand{\gpgfxscale}{1}
\RequirePackage{geometry}
\RequirePackage[explicit]{titlesec}
\RequirePackage{array}
\RequirePackage{graphicx, hyperref, longtable}
\RequirePackage{colortbl}
\RequirePackage{suffix}
\RequirePackage[activate={true,nocompatibility},final,tracking=true,kerning=true,spacing=true,factor=1100,stretch=10,shrink=10]{microtype}

%Layout Commands
\newcommand{\htmlparbreak}{}
\newenvironment{htmlpar}{\begin{flushleft}}{\end{flushleft}}

%Style Commands
\newcommand{\theend}{\begin{center}\textit{THE END}\end{center}}

\newcommand{\headingfont}{\sffamily\itshape\bfseries}
\newcommand{\headingcolor}{\color{kulerE}}

\titleformat{\part}%Command
[display]%Shape
{\Huge\headingfont\headingcolor}%Format of Title
{\parttitlename\ \thepart}%Sectioning Label
{.2em}%Horizontal Separation
{#1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\chapter}%Command
[display]%Shape
{\huge\headingfont\headingcolor}%Format of Title
{\chaptertitlename\ \thechapter}%Sectioning Label
{.2em}%Horizontal Separation
{#1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\section}%Command
[hang]%Shape
{\Large\headingfont\headingcolor}%Format of Title
{\thesection}%Sectioning Label
{.2em}%Horizontal Separation
{\ #1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\subsection}%Command
[hang]%Shape
{\large\headingfont\headingcolor}%Format of Title
{\thesubsection}%Sectioning Label
{.2em}%Horizontal Separation
{\ #1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\subsubsection}%Command
[hang]%Shape
{\normalsize\headingfont\headingcolor}%Format of Title
{\thesubsubsection}%Sectioning Label
{0em}%Horizontal Separation
{\ #1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\paragraph}%Command
[runin]%Shape
{\normalsize\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{0em}%Horizontal Separation
{#1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\subparagraph}%Command
[runin]%Shape
{\normalsize\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{6em}%Horizontal Separation
{#1}%Code preceding Title Body
[]%Code following the Title Body

\RequirePackage{tikz}
\usetikzlibrary{shapes,shadows,calc}
\usetikzlibrary{positioning,calc}

%------------------------------------
%INVERTED TRIANGLE HEADLINES
\newcommand\tikzTriangleTitle[2]{%
\begin{tikzpicture}
\node (headline) [rectangle,%
            minimum width=\textwidth,%
            minimum height=1.4em,%
            color=white,%
            fill=kulerC,%
            text width=\textwidth-5em,%
            align=left]%
    {~\hspace{2em}\parbox{\textwidth}{#1}}%HEADLINE
;
\fill[fill=kulerB]  (headline.north west) --%
                    ($(headline.north west)+(2.5em,0)$) --%
                    ($0.5*(headline.north west)-0.5*(headline.north west)-(0.5*\textwidth-4em,0)$)--%
                    ($(headline.south west)+(2.5em,0)$)--%
                    (headline.south west);
\node [color=white](headline.north west)%
                    at ($0.5*(headline.north west)-0.5*(headline.north west)-(0.5*\textwidth-2em,0)$)%
                    {#2};%NUMBERING
\end{tikzpicture}
}
\newcommand\tikzTriangleChapterTitle[2]{%
\begin{tikzpicture}
\node (headline) [rectangle,%
minimum width=\textwidth,%
minimum height=1.4em,%
color=white,%
fill=kulerC,%
text width=\textwidth-5em,%
align=left]%
{~\hspace{2em}\parbox{\textwidth}{#1}}%HEADLINE
;
\fill[fill=kulerB,inner sep=0pt]  (current page.north west |- headline.north west) --%
($(headline.north west)+(2.5em,0)$) --%
($0.5*(headline.north west)-0.5*(headline.north west)-(0.5*\textwidth-4em,0)$)--%
($(headline.south west)+(2.5em,0)$)--%
(current page.north west |- headline.south west);
\node [color=white](headline.north west)%
at ($0.5*(headline.north west)-0.5*(headline.north west)-(0.5*\textwidth-2em,0)$)%
{#2};%NUMBERING
\end{tikzpicture}
}
\newcommand{\triangleinvertedheadlines}{%
%\titleformat{\section}%
%{\normalfont}{}{0em}%
%{\tikzSecTriangleTitle{east}{west}{0\paperwidth}{##1}}
\titleformat{\part}%Command
[display]%Shape
{\Huge\headingfont\headingcolor}%Format of Title
{\parttitlename\ \thepart}%Sectioning Label
{.2em}%Horizontal Separation
{##1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\chapter}%Command
[display]%Shape
{\huge\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{.2em}%Horizontal Separation
{\tikzTriangleChapterTitle{##1}{\chaptertitlename\ \thechapter}}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\section}%Command
[hang]%Shape
{\Large\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{.2em}%Horizontal Separation
{\tikzTriangleTitle{##1}{\thesection}}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\subsection}%Command
[hang]%Shape
{\large\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{.2em}%Horizontal Separation
{\tikzTriangleTitle{##1}{\thesubsection}}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\subsubsection}%Command
[hang]%Shape
{\normalsize\headingfont\headingcolor}%Format of Title
{\thesubsubsection}%Sectioning Label
{0em}%Horizontal Separation
{\ ##1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\paragraph}%Command
[runin]%Shape
{\normalsize\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{0em}%Horizontal Separation
{##1}%Code preceding Title Body
[]%Code following the Title Body

\titleformat{\subparagraph}%Command
[runin]%Shape
{\normalsize\headingfont\headingcolor}%Format of Title
{}%Sectioning Label
{0em}%Horizontal Separation
{##1}%Code preceding Title Body
[]%Code following the Title Body
}

%Chapterauthor Code from stackexchange (modified)
\newcommand\chapterauthor[1]{\authortoc{#1}\printchapterauthor{#1}}
\WithSuffix\newcommand\chapterauthor*[1]{\printchapterauthor{#1}}

\makeatletter
\newcommand{\printchapterauthor}[1]{%
{\parindent0pt\vspace*{-25pt}%
\linespread{1.1}\large\scshape#1%
\par\nobreak\vspace*{35pt}}
\@afterheading%
}
\newcommand{\authortoc}[1]{%
\addtocontents{toc}{\vskip-10pt}%
\addtocontents{toc}{%
\protect\contentsline{chapter}%
{\hskip1.3em\mdseries\scshape\protect\scriptsize#1}{}{}}
\addtocontents{toc}{\vskip5pt}%
}
\makeatother

\newcommand{\chapterdate}[1]{\begin{flushright}\textit{#1}\end{flushright}}%TODO TOC Integration


%Column Formats for Line Breaking Support
\newcolumntype{L}[1]{>{\raggedright\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{R}[1]{>{\raggedleft\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}
\newcolumntype{T}[1]{>{\raggedright\let\newline\\\arraybackslash}p{#1}}

%Timeline Table
\newcommand{\mktimelinebullet}{\makebox[0pt]{\textbullet}\hskip-0.5pt\vrule width 1pt\hspace{\labelsep}}
\newcommand{\timelineentry}[3]{\timelineentrynolink{#1}{\hyperref[#3]{#2}}}
\newcommand{\timelineentrynolink}[2]{\timelinemanualcolumn{#1}{\multicolumn{2}{l}{#2} &}}
\newcommand{\timelinesecondaryentry}[3]{\timelinesecondaryentrynolink{#1}{\hyperref[#3]{#2}}}
\newcommand{\timelinesecondaryentrynolink}[2]{\timelinemanualcolumn{#1}{& \multicolumn{2}{l}{#2}}}
\newcommand{\timelinemanualcolumn}[2]{#1 & #2\\}
\newenvironment{timelinetable}[1]{%
\begin{table}
\renewcommand\arraystretch{1.4}%
%\captionsetup{singlelinecheck=false, font=blue, labelfont=sc, labelsep=quad}%
%\caption{#1}\vskip -1.5ex%
\begin{longtable}%
{@{\,}r <{\hskip 2pt} !{\mktimelinebullet} T{0.15\textwidth} T{0.35\textwidth} T{0.25\textwidth}}%
%\toprule
\addlinespace[1.5ex]%
}{%
\end{longtable}
\end{table}%
}

%Provide graphics to be added at the margin of the page
\newlength{\margingraphicswidth}
\setlength{\margingraphicswidth}{\marginparwidth}
\newcommand{\margingraphics}[1]{\marginpar{\raisebox{-0.5\margingraphicswidth}{\includegraphics[width=\margingraphicswidth]{#1}}}}

%Provide Journal Headings with a calendar thumbnail
\RequirePackage{calc,graphicx}
\newlength\nodeheight
\setlength{\nodeheight}{3em}
\newlength\nodewidth
\setlength{\nodewidth}{3em}
\newlength\dayboxwidth
\newsavebox{\mybox}

\newcommand{\journalcalendar}[2]{%
%\sbox{\mybox}{\parbox{\nodewidth}{\color{white}{\sffamily\textbf{\MakeUppercase{#1}}}}}%
%\settowidth\dayboxwidth{\mybox}
%\pgfmathparse{\dayboxwidth/\nodewidth}
%\begin{tikzpicture}
%%resize your box to fit inside node; keep width-to-height ratio using !, and resize height as necessary
%\node [rectangle, minimum width=\nodewidth, minimum height=\nodeheight, fill=red] {\resizebox{!}{\pgfmathresult\nodewidth}{\usebox{\mybox}}};
%\end{tikzpicture}
%}
\begin{tikzpicture}
\fill[red, text=white, rounded corners=1em] (0,0) rectangle (2,1) node [pos=.5] () {\sffamily\textbf{\MakeUppercase{#1}}};
\end{tikzpicture}
}
\RequirePackage{gpmain} % Automatically include GP Main Package

\endinput