FROM debian:stretch
LABEL maintainer="Lukas Kirschner <kirschlu@googlemail.com>"
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8 TERM='xterm-256color'
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get --yes --no-install-recommends update && \
    apt-get --yes --no-install-recommends upgrade && \
    apt-get --yes --no-install-recommends install fontconfig texlive-full latexmk poppler-utils openjdk-8-jre-headless gradle libfile-copy-recursive-perl xzdec git && \
    apt-get --purge remove -y .\*-doc$

WORKDIR /home

RUN git clone https://gitlab.com/lukaslsm/lk-latex-templates.git gptex && \
    cd gptex && \
    ./generate_texlsr_without_gradle.sh && \
    mkdir -p $(kpsewhich --var-value TEXMFHOME)/tex && \
    cp -v -R texmf/tex $(kpsewhich --var-value TEXMFHOME)/tex && \
    cd .. && \
    rm -rf gptex && \
    mktexlsr

RUN kpsewhich gpclass.cls && \
    kpsewhich stmaryrd.sty && \
    kpsewhich beamer.cls && \
    which gradle && \
    true
